# language: pt

Funcionalidade: Acessa a página de Fotogalerias do Jornal O Globo

    Cenário: Usuário acessa a Fotogalerias do Jornal O Globo
        Dado que sou usuário
        Quando acesso a página de fotogalerias
        Entao visualizo a página de fotogalerias