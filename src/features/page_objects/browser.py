from selenium import webdriver
from base import Base
from my.util import Settings

class Browser(object):
    def getInstance(self,context,browserProfile = None):

        if browserProfile == None:
            browserProfile = webdriver.FirefoxProfile()

        if context.driver == None:
            binary = webdriver.firefox.firefox_binary.FirefoxBinary('../firefox_v34_0/firefox')
            context.driver =  webdriver.Firefox(firefox_profile=browserProfile,firefox_binary=binary)
            return context.driver
        else:
            return context.driver
        
    def open(self,context,url):
        self.getInstance(context).get(url)
