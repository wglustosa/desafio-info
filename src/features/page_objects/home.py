# coding: UTF-8
# language: pt
from selenium import webdriver
from base import Base
from my.util import Settings
from selenium.webdriver.common.keys import Keys
from time import sleep

class Home(Base):
    context = None
    url = Settings.BASE_URL_FRONT
    
    def __init__(self,context):
        Base.__init__(self,context,self.browserProfile(context))
        self.context = context
    
    def browserProfile(self,context):
        fp = webdriver.FirefoxProfile()
        fp.add_extension(extension='../modify_headers-0.7.1.1-fx.xpi')
        
        if context.identified == False:
            self.msisdn = ''
        
        fp.set_preference("modifyheaders.config.active", True)
        fp.set_preference("modifyheaders.config.alwaysOn", True)
        fp.set_preference("modifyheaders.headers.count", 1)
        fp.set_preference("modifyheaders.headers.action0", "Add")
        fp.set_preference("modifyheaders.headers.name0", "msisdn")
        fp.set_preference("modifyheaders.headers.value0", self.msisdn)
        fp.set_preference("modifyheaders.headers.enabled0", True)
        
        return fp
    
    def acessa(self):
        self.browser.open(self.context,self.url)
        self.context.driver.maximize_window()
        self.context.driver.set_window_size(360, 640)
        
    def visualizaConteudos(self):
        sleep(3)
        conteudos = self.context.driver.find_elements_by_class_name('web-contents')
        return (len(conteudos) >= 1)
    
    def visualizaMaisConteudos(self):
        sleep(6)
        conteudos = self.context.driver.find_elements_by_class_name('web-contents')
        return (len(conteudos) >= 6)